#! /bin/bash

# tasks is updated, touch scripts.d to run it
ansible -b -i ./../deploy/hosts ./../deploy/local.yml
ansible -b -i ./../deploy/hosts ./../deploy/provision.yml

supervisorctl reread
supervisorctl update