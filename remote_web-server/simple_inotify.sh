#! /bin/bash
while inotifywait -qre IN_MOVED_TO,CREATE,MODIFY /tmp/ansible-repo/scripts.d
do
        for SCRIPT in /tmp/ansible-repo/scripts.d/*
        do
        	chmod +x $SCRIPT
        	if [ -f $SCRIPT -a -x $SCRIPT ]
        		then 
        		$SCRIPT
        	fi
        done
done